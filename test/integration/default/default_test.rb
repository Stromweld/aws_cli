# InSpec test for recipe xe_awscli::default

# The InSpec reference, with examples and extensive documentation, can be
# found at https://www.inspec.io/docs/reference/resources/

if os.windows?
  describe package('AWS Command Line Interface v2') do
    it { should be_installed }
  end
else
  describe file('/usr/local/bin/aws') do
    it { should exist }
  end
end
