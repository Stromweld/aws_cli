# aws_cli CHANGELOG

This file is used to list changes made in each version of the aws_cli cookbook.

## 1.0.0 (2023-09-25)

- [Corey Hemminger] - Initial release.
