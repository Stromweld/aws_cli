name 'aws_cli'
maintainer 'Corey Hemminger'
maintainer_email 'hemminger@hotmail.com'
license 'Apache-2.0'
description 'Installs/Configures awscli v2+'
version '1.0.0'
chef_version '>= 15.3'

issues_url 'https://gitlab.com/Stromweld/aws_cli/issues'
source_url 'https://gitlab.com/Stromweld/aws_cli'

%w(redhat centos amazon).each do |os|
  supports os
end

depends 'ark'
