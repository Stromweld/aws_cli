# To learn more about Custom Resources, see https://docs.chef.io/custom_resources.html
#
# Author:: Corey Hemminger
# Cookbook:: aws_cli
# Resource:: install
#
# Copyright:: 2023, Corey Hemminger
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
unified_mode true

description 'Install AWS CLI v2+'

property :download_url, String,
         description: 'url to download the awscli zip file'

action :install do
  description 'Perform the Installation'

  download_url = if new_resource.download_url
                   new_resource.download_url
                 elsif platform_family?('windows')
                   'https://awscli.amazonaws.com/AWSCLIV2.msi'
                 else
                   'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip'
                 end

  if node['os'].include?('linux')
    include_recipe 'ark'

    ark 'awscli' do
      url download_url
      path Chef::Config[:file_cache_path].to_s
      action :put
      notifies :run, 'execute[install awscli]', :immediately
    end

    execute 'install awscli' do
      command "#{Chef::Config[:file_cache_path]}/awscli/install"
      creates '/usr/local/bin/aws'
    end

  elsif node['os'].include?('windows')
    remote_file 'download awscli msi' do
      source download_url
      path "#{Chef::Config[:file_cache_path]}/awscli.msi"
    end

    package "#{Chef::Config[:file_cache_path]}/awscli.msi"

  else
    raise 'OS unsupported yet'
  end
end

action :remove do
  description 'Remove AWS CLI from server'

  package "#{Chef::Config[:file_cache_path]}/awscli.msi" do
    action :remove
  end
end
